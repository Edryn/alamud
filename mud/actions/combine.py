# -*- coding: utf-8 -*-
# Copyright (C) 2015 Kevin Robin, IUT d'Orléans
#==============================================================================

from .action import Action3
from mud.events import PreCombineEvent

class CombineAction(Action3):
    EVENT = PreCombineEvent
    RESOLVE_OBJECT = "resolve_for_use"
    RESOLVE_OBJECT2 = "resolve_for_operate"
    ACTION = "combine"