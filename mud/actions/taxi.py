# -*- coding: utf-8 -*-
# Copyright (C) 2015 Mathieu HIREL, IUT d'Orléans
#==============================================================================

from .action import Action1, Action2
from mud.events import TaxiApproachEvent, PullEvent, GoToEvent
from mud.models.mixins.containing import Containing
from mud.models                   import Player
import mud.game

class CallTaxiAction(Action1):
	EVENT = TaxiApproachEvent
	ACTION = "CallTaxi"


class PullAction(Action2):
  EVENT = PullEvent
  ACTION = "Pull"
  RESOLVE_OBJECT = "resolve_for_use"

  def resolve_object(self):
    meth = getattr(self, "RESOLVE_OBJECT")
    obj = getattr(self.subject, meth)(name=self.object)
    if obj is not None:
      return obj
    location = self.subject.container()
    if location.has_prop("taxi-here"):
      world = mud.game.GAME.world
      taxi = world.get('taxi-000')
      obj = self.find_object_on_location(name=self.object, location=taxi)
      if obj is not None:
        return obj
    else:
      obj = self.find_object_on_location(name=self.object, location=location)
      if obj is not None:
        return obj
    return None


  #Fonction du Player (Oui j'assume le copier coller :p)
  def _make_find_pred(self, kargs):
      """create a function to test whether an object matches the given
      criteria."""
      test = kargs.get("test")          # a function           (optional)
      name = kargs.get("name")          # a name               (optional)
      prop = kargs.get("prop")          # a property           (optional)
      props= kargs.get("props")         # a list of properties (optional)
      def pred(x):                      # the new testing predicate
          return (((not test)  or text(x))          and
                  ((not name)  or x.has_name(name)) and
                  ((not prop)  or x.has_prop(prop)) and
                  ((not props) or x.has_props(props)))

      return pred

  def find_object_on_location(self, **kargs):
        """find an object that you can use/drop:
        - in your inventory"""
        location = kargs.get('location')
        pred = self._make_find_pred(kargs)
        for x in location.all():
            if pred(x):
                return x
        return None

class GoToAction(Action2):
  EVENT = GoToEvent
  ACTION = "GoTo"

  def resolve_object(self):
          world = mud.game.GAME.world
          loc = world.get(self.object)
          if loc:
              locs = [loc]
          else:
              locs = []
              for k,v in world.items():
                  if isinstance(v, Containing) and \
                     not isinstance(v, Player) and \
                     k.find(self.object) != -1:
                      locs.append(v)
          return locs  