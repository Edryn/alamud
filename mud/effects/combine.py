# -*- coding: utf-8 -*-
# Copyright (C) 2015 Kevin Robin, IUT d'Orléans
#==============================================================================

from .effect import Effect3
from mud.events import CombineEvent

class CombineEffect(Effect3):
    EVENT = CombineEvent
    
    def make_event(self):
        o1 = self.resolve('o1')
        o2 = self.resolve('o2')
        res = self.yaml['res']
        e = self.EVENT(self.actor, o1, o2)
        e.res = res
        return e