# -*- coding: utf-8 -*-
# Copyright (C) 2014 Mathieu HIREL, IUT d'Orléans
#==============================================================================

from .effect import Effect1
from mud.events import EnterTaxiEvent, ExitTaxiEvent

class EnterTaxiEffect(Effect1):
    EVENT = EnterTaxiEvent

    # def resolve_actor(self):
    #     return self.resolve("to")

    def make_event(self):
        e = super().make_event()
        e.direction = self.resolve("to")

        return e


class ExitTaxiEffect(Effect1):
    EVENT = ExitTaxiEvent

    # def resolve_actor(self):
    #     return self.resolve("to")

    def make_event(self):
        e = super().make_event()

        return e
